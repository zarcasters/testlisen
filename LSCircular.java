package listasEnlazadas;

public class LSCircular extends LSimple {

    public LSCircular() {
        super();
    }

    public boolean esVacia() {
        return (super.esVacia());
    }

    public NodoS getH() {
        return h;
    }

    public void setH(NodoS h) {
        this.h = h;
    }

    public void adiFin(Object o) {
        NodoS nn = new NodoS();
        nn.setO(o);
        if (!esVacia()) {
            NodoS i = h;
            while (i.getN() != h) {
                i = i.getN();
            }
            i.setN(nn);
            nn.setN(h);
        } else {
            nn.setN(nn);
            this.h = nn;
        }
    }

    public void adiPrimero(Object o) {
        NodoS nn = new NodoS();
        nn.setO(o);
        if (!esVacia()) {
            nn.setN(h);
            NodoS i = h;
            while (i.getN() != h) {
                i = i.getN();
            }
            i.setN(nn);
        } else {
            nn.setN(nn);
        }
        this.h = nn;
    }

    public Object eliFin() {
        Object o = new Object();
        if (!esVacia()) {
            NodoS i, ii = new NodoS();
            i = h;
            while (i.getN() != h) {
                ii = i;
                i = i.getN();
            }
            o = i.getO();
            if (i == h) {
                this.h = null;
            } else {
                ii.setN(h);
            }
        } else {
            System.out.println("Lista vacía...");
        }
        return o;
    }

    public Object eliPrimero() {
        Object o = new Object();
        if (!esVacia()) {
            o = h.getO();
            NodoS i = h;
            while (i.getN() != h) {
                i = i.getN();
            }
            if (i == h) {
                this.h = null;
            } else {
                this.h = h.getN();
                i.setN(h);
            }
        } else {
            System.out.println("Lista vacía...");
        }
        return o;
    }

    public int nElem() {
        int c = 0;
        if (!esVacia()) {
            NodoS i = h;
            c = 1;
            while (i.getN() != h) {
                c++;
                i = i.getN();
            }
        }
        return c;
    }

    public void mostrar() {
        if (!esVacia()) {
            int s = nElem();
            NodoS i = h;
            for (int j = 1; j <= s; j++) {
                System.out.print("\t" + i.getO());
                i = i.getN();
            }
        } else {
            System.out.println("Lista vacía...");
        }
        System.out.println("");
    }

}
