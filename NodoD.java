package listasEnlazadas;

public class NodoD {

    private NodoD p, n;
    private Object o;

    public NodoD() {
        this.p = null;
        this.n = null;
    }

    public NodoD getP() {
        return p;
    }

    public NodoD getN() {
        return n;
    }

    public Object getO() {
        return o;
    }

    public void setP(NodoD p) {
        this.p = p;
    }

    public void setN(NodoD n) {
        this.n = n;
    }

    public void setO(Object o) {
        this.o = o;
    }

}
