package listasEnlazadas;

public class LDCircular extends LDoble {

    public LDCircular() {
        super();
    }

    public boolean esVacia() {
        return (super.esVacia());
    }

    public NodoD getH() {
        return h;
    }

    public void setH(NodoD h) {
        this.h = h;
    }

    public void adiFin(Object o) {
        NodoD nn = new NodoD();
        nn.setO(o);
        if (!esVacia()) {
            nn.setN(h);
            nn.setP(h.getP());
            this.h.setP(nn);
            nn.getP().setN(nn);
            
        } else {
            nn.setP(nn);
            nn.setN(nn);
            this.h = nn;
        }
    }

    public void adiPrimero(Object o) {
        NodoD nn = new NodoD();
        nn.setO(o);
        if (!esVacia()) {
            nn.setN(h);
            nn.setP(h.getP());
            this.h.setP(nn);
            nn.getP().setN(nn);
        } else {
            nn.setP(nn);
            nn.setN(nn);
        }
        this.h = nn;
    }

    public Object eliFin() {
        Object o = new Object();
        if (!esVacia()) {
            NodoD nn = new NodoD();
            nn = h.getP();
            o = nn.getO();
            if (h == nn) {
                this.h = null;
            } else {
                nn.getP().setN(h);
                h.setP(nn.getP());
            }
        } else {
            System.out.println("Lista vacía...");
        }
        return o;
    }

    public Object eliPrimero() {
        Object o = new Object();
        if (!esVacia()) {
            o = h.getO();
            if (h.getP() == h) {
                this.h = null;
            } else {
                h.getP().setN(h.getN());
                h.getN().setP(h.getP());
                this.h = h.getN();
            }
        } else {
            System.out.println("Lista vacía...");
        }
        return o;
    }

    public int nElem() {
        int c = 0;
        if (!esVacia()) {
            NodoD i = new NodoD();
            i = h;
            c = 1;
            while (i.getN() != h) {
                i = i.getN();
                c++;
            }
        }
        return c;
    }

    public void mostrar() {
        if (!esVacia()) {
            int s = nElem();
            NodoD i = new NodoD();
            i = h;
            for (int j = 1; j <= s; j++) {
                System.out.print("\t" + i.getO());
                i = i.getN();
            }
        } else {
            System.out.println("Lista vacía...");
        }
        System.out.println("");
    }

}
