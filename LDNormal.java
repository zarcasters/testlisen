package listasEnlazadas;

public class LDNormal extends LDoble {

    public LDNormal() {
        super();
    }

    public boolean esVacia() {
        return super.esVacia();
    }

    NodoD getH() {
        return h;
    }

    void setH(NodoD h) {
        this.h = h;
    }

    public void adiFin(Object o) {
        NodoD nn = new NodoD();
        nn.setO(o);
        if (!esVacia()) {
            NodoD i = h;
            while (i.getN() != null) {
                i = i.getN();
            }
            nn.setP(i);
            i.setN(nn);
        } else {
            this.h = nn;
        }
    }

    public void adiPrimero(Object o) {
        NodoD nn = new NodoD();
        nn.setO(o);
        if (!esVacia()) {
            nn.setN(h);
            h.setP(nn);
        }
        this.h = nn;
    }

    public Object eliFin() {
        Object o = new Object();
        if (!esVacia()) {
            NodoD i = h;
            while (i.getN() != null) {
                i = i.getN();
            }
            o = i.getO();
            if (i == h) {
                this.h = null;
            } else {
                i.getP().setN(null);
            }
        } else {
            System.out.print("Lista vacía...");
        }
        return o;
    }

    public Object eliPrimero() {
        Object o = new Object();
        if (!esVacia()) {
            o = h.getO();
            if (h.getN() != null) {
                this.h = h.getN();
                h.setP(null);
            } else {
                this.h = null;
            }
        } else {
            System.out.println("Lista vacía...");
        }
        return o;
    }

    public int nElem() {
        int c = 0;
        if (!esVacia()) {
            c = 1;
            NodoD i = h;
            while (i.getN() != null) {
                i = i.getN();
                c++;
            }
        }
        return c;
    }

    public void mostrar() {
        if (!esVacia()) {
            NodoD i = h;
            while (i != null) {
                System.out.print("\t" + i.getO());
                i = i.getN();
            }
        } else {
            System.out.println("Lista vacía...");
        }
    }

}
