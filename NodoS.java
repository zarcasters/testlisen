package listasEnlazadas;

public class NodoS {

    private NodoS n;
    private Object o;

    public NodoS() {
        this.n = null;
    }

    public NodoS getN() {
        return n;
    }

    public Object getO() {
        return o;
    }

    public void setN(NodoS n) {
        this.n = n;
    }

    public void setO(Object o) {
        this.o = o;
    }

}
