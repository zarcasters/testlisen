package listasEnlazadas;

public abstract class LSimple {

    protected NodoS h;

    public LSimple() {
        this.h = null;
    }

    public boolean esVacia() {
        if (h == null) {
            return true;
        } else {
            return false;
        }
    }

    public abstract void adiFin(Object o);

    public abstract void adiPrimero(Object o);

    public abstract Object eliFin();

    public abstract Object eliPrimero();

    public abstract int nElem();

    public abstract void mostrar();

}
