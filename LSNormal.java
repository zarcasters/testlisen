package listasEnlazadas;

public class LSNormal extends LSimple {

    public LSNormal() {
        super();
    }

    public boolean esVacia() {
        return (super.esVacia());
    }

    public NodoS getH() {
        return h;
    }

    public void setH(NodoS h) {
        this.h = h;
    }

    public void adiFin(Object o) {
        NodoS nn = new NodoS();
        nn.setO(o);
        if (!esVacia()) {
            NodoS i = h;
            while (i.getN() != null) {
                i = i.getN();
            }
            i.setN(nn);
        } else {
            this.h = nn;
        }
    }

    public void adiPrimero(Object o) {
        NodoS nn = new NodoS();
        nn.setO(o);
        nn.setN(h);
        this.h = nn;
    }

    public Object eliFin() {
        Object o = new Object();
        if (!esVacia()) {
            NodoS i, ii = new NodoS();
            i = h;
            while (i.getN() != null) {
                ii = i;
                i = i.getN();
            }
            o = i.getO();
            if (i == h) {
                this.h = null;
            } else {
                ii.setN(null);
            }
        } else {
            System.out.println("Lista vacía...");
        }
        return o;
    }

    public Object eliPrimero() {
        Object o = new Object();
        if (!esVacia()) {
            o = h.getO();
            this.h = h.getN();
        } else {
            System.out.println("Lista vacía...");
        }
        return o;
    }

    public int nElem() {
        int c = 0;
        if (!esVacia()) {
            NodoS i = h;
            c = 1;
            while (i.getN() != null) {
                c++;
                i = i.getN();
            }
        }
        return c;
    }

    public void mostrar() {
        if (!esVacia()) {
            NodoS i = h;
            while (i != null) {
                System.out.print("\t" + i.getO());
                i = i.getN();
            }

        } else {
            System.out.println("Lista vacía...");
        }
        System.out.println("");
    }
    
    public boolean buscar(Object o) {
        boolean f = false;
        NodoS i = h;
        while (i != null) {
            if (i.getO() == o) {
                f = true;
                break;
            }
            i = i.getN();
        }
        return f;
    }

    public void ordenar() {
        LSNormal la = new LSNormal();
        while (!esVacia()) {
            boolean f=false;
            Object o = new Object();
            o = eliPrimero();
            NodoS i = h;
            while (i != null) {
                if(i.getO().hashCode()<o.hashCode()){
                    f=true;
                    break;
                }
                i=i.getN();
            }
            if(f){
                adiFin(o);
            }else{
                la.adiFin(o);
            }
        }
        while(!la.esVacia()){
            adiFin(la.eliPrimero());
        }
    }

}
